ifeq ($(DEBUG), 1)
	DEBUG_FLAGS=-O0 -g
endif

all: client server

client: src/client.c src/game.c
	gcc src/client.c src/game.c -o client $(DEBUG_FLAGS) -Wall -pthread

server: src/server.c src/game.c
	gcc src/server.c src/game.c -o server $(DEBUG_FLAGS) -Wall

clean:
	rm -f client server

.PHONY: clean
