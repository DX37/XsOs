#include "game.h"

void perr(char* err)
{
    perror(err);
    exit(EXIT_FAILURE);
}

void print_grid(int** grid)
{
    printf("  1 2 3 y\n");
    for (int i = 0; i < 3; ++i)
    {
        printf("%d ", i + 1);
        for (int j = 0; j < 3; ++j)
        {
            if (grid[i][j] == X)
                printf("\E[1;34mX\E[0m "); // Print blue X
            else if (grid[i][j] == O)
                printf("\E[1;31mO\E[0m "); // Print red O
            else if (grid[i][j] == EMPTY)
                printf("\E[1;37m0\E[0m "); // Print white zero
            else
                printf("? "); // Undefined cell value
        }
        printf("\n");
    }
    printf("x\n");
}

int set_grid_val(int** grid, int x, int y, int val)
{
    if (grid[x][y] != EMPTY)
        return -1;
    grid[x][y] = val;
    return 0;
}

int check_set_coords(int x, int y)
{
    if ((x < 0 || x > 2) || (y < 0 || y > 2))
        return -1;
    return 0;
}

int** alloc_grid()
{
    int **temp_grid = calloc(3, sizeof(int *));
    for (int i = 0; i < 3; ++i)
        temp_grid[i] = calloc(3, sizeof(int));

    return temp_grid;
}

void dealloc_grid(int** grid)
{
    for (int i = 0; i < 3; ++i)
        free(grid[i]);
    free(grid);
}

int check_grid_condition(int** grid)
{
    for (int k = X; k <= O; ++k)
    {
        int count = 0;
        for (int i = 0; i < 3; ++i)
        {
            if (grid[i][i] == k)
                ++count;
            if (count == 3)
                return k;
        }

        count = 0;
        int j = 2;
        for (int i = 0; i < 3; ++i)
        {
            if (grid[i][j] == k)
                ++count;
            if (count == 3)
                return k;
            --j;
        }

        for (int i = 0; i < 3; ++i)
        {
            count = 0;
            for (int j = 0; j < 3; ++j)
            {
                if (grid[i][j] == k)
                    ++count;
            }
            if (count == 3)
                return k;
        }

        for (int i = 0; i < 3; ++i)
        {
            count = 0;
            for (int j = 0; j < 3; ++j)
            {
                if (grid[j][i] == k)
                    ++count;
            }
            if (count == 3)
                return k;
        }
    }
    return EMPTY;
}
