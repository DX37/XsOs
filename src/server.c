#include "game.h"

#define MAX_EVENTS 100

typedef struct
{
    int fd_X;
    int fd_O;
    int move;
} room;

int sfd;
int rooms_amount = 0, remote_rooms_amount = 0, rooms_max;
int ***game_grid;
room *rooms;
int server_number, servers_amount;
list *servers;

int init_conn(char* ip, int port)
{
    struct sockaddr_in sa;
    int ssfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    printf("ssfd: %d\n", ssfd);
    if (ssfd == -1)
    {
        perror("socket");
        return -1;
    }

    memset(&sa, 0, sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);

    if (inet_pton(AF_INET, ip, &sa.sin_addr) != 1)
    {
        perror("inet_pton");
        close(ssfd);
        return -1;
    }

    if (connect(ssfd, (struct sockaddr *)&sa, sizeof(sa)) != 0)
    {
        printf("ERROR: server is not present at specified address\n");
        perror("connect");
        close(ssfd);
        return -1;
    }

    return ssfd;
}

void send_msg_to_reserve(int socket, int server)
{
    int msg = SERVER;
    send(socket, &msg, sizeof(int), 0);
    send(socket, &rooms_amount, sizeof(int), 0);
    send(socket, rooms, sizeof(*rooms) * rooms_amount, 0);
    printf("Sent %d rooms to %d server.\n", rooms_amount, server + 1);
    for (int k = 0; k < rooms_amount; ++k)
        printf("rooms[%d] = {fd_X: %d, fd_O: %d, move: %d}\n", k, rooms[k].fd_X, rooms[k].fd_O, rooms[k].move);
    for (int k = 0; k < rooms_amount; ++k)
    {
        int sent = send(socket, game_grid[k][0], 3 * 3 * sizeof(int*) + sizeof(int), 0);
        printf("Sent grid (%d bytes) to %d server.\n", sent, server + 1);
    }
    perror("send");

    close(socket);
}

void sync_send()
{
    int ssfd;
    if (servers[server_number].type == 1 && (server_number + 1 < servers_amount))
    {
        ssfd = init_conn(servers[0].ip, servers[0].port);
        if (ssfd == -1)
        {
            printf("Cannot connect to main server. Trying sync_send to next reserve server.\n");
            close(ssfd);
        }

        ssfd = init_conn(servers[server_number + 1].ip, servers[server_number + 1].port);
        if (ssfd == -1)
            return;

        send_msg_to_reserve(ssfd, server_number + 1);
        return;
    }
    for (int i = server_number; i < servers_amount; ++i)
        if ((servers[i].type == 1) && (servers[server_number].type == 0))
        {
            ssfd = init_conn(servers[i].ip, servers[i].port);
            if (ssfd == -1)
                return;

            send_msg_to_reserve(ssfd, i);
        }
}

void send_msg_all(int* buf)
{
    for (int k = 0; k < rooms_max; ++k)
    {
        if (rooms[k].fd_X > 0)
            send(rooms[k].fd_X, buf, sizeof(int), 0);
        if (rooms[k].fd_O > 0)
            send(rooms[k].fd_O, buf, sizeof(int), 0);
    }
}

void exit_handle()
{
    int msg = STOP;
    send_msg_all(&msg);

    close(sfd);

    for (int i = 0; i < rooms_max; ++i)
        dealloc_grid(game_grid[i]);

    printf("\nTerminated.\n");
}

void sig_handle()
{
    exit(0);
}

int main(int argc, char** argv)
{
    server_number = (argc > 1) ? atoi(argv[1]) - 1 : -1;
    FILE *server_list = fopen("server_list", "r");
    if (server_list == NULL)
    {
        printf("server_list not found!");
        return 0;
    }

    for (servers_amount = 0; !feof(server_list); ++servers_amount)
        fscanf(server_list, "%*s\n");
    printf("Servers amount: %d\n", servers_amount);

    servers = calloc(servers_amount, sizeof(*servers));
    rewind(server_list);
    for (int i = 0; i < servers_amount; ++i)
    {
        fscanf(server_list, "%[^:]:%d:%d\n", servers[i].ip, &servers[i].port, &servers[i].type);
        if (server_number == i)
            printf("%d) %s:%d (type: %d) <<<\n", i + 1, servers[i].ip, servers[i].port, servers[i].type);
        else
            printf("%d) %s:%d (type: %d)\n", i + 1, servers[i].ip, servers[i].port, servers[i].type);
    }
    printf("\n");
    fclose(server_list);

    if (server_number == -1 || server_number >= servers_amount)
    {
        printf("Enter number of server.\n");
        return 0;
    }

    rooms_max = (argc > 2) ? atoi(argv[2]) : 1;
    rooms = calloc(rooms_max, sizeof(*rooms));
    game_grid = calloc(rooms_max, sizeof(int***));
    for (int i = 0; i < rooms_max; ++i)
    {
        rooms[i].fd_X = -1;
        rooms[i].fd_O = -1;
        game_grid[i] = alloc_grid();
    }

    printf("Max games: %d\n", rooms_max);

    struct epoll_event ev, events[MAX_EVENTS];
    struct sockaddr_in sa;

    sfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sfd == -1)
        perr("socket");
    int epollfd;

    int option = 1;
    if (setsockopt(sfd, SOL_SOCKET, (SO_REUSEPORT | SO_REUSEADDR), (char*)&option, sizeof(option)) == -1)
        perr("setsockopt");

    signal(SIGINT, sig_handle);
    signal(SIGTERM, sig_handle);
    signal(SIGHUP, sig_handle);
    atexit(exit_handle);

    printf("Port: %d\n", servers[server_number].port);
    printf("sfd: %d\n", sfd);
    memset(&sa, 0, sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_port = htons(servers[server_number].port);
    sa.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(sfd, (struct sockaddr *)&sa, sizeof(sa)) == -1)
        perr("bind");

    if (listen(sfd, 10) == -1)
        perr("listen");

    epollfd = epoll_create1(0);
    if (epollfd == -1)
        perr("epoll_create1");

    ev.events = EPOLLIN;
    ev.data.fd = sfd;
    if (epoll_ctl(epollfd, EPOLL_CTL_ADD, sfd, &ev) == -1)
        perr("epoll_ctl");

    while (1)
    {
        int nfds = epoll_wait(epollfd, events, MAX_EVENTS, -1);
        if (nfds == -1)
            perr("epoll_wait");

        for (int i = 0; i < nfds; ++i)
        {
            char str[500];
            if (events[i].data.fd == sfd)
            {
                int msg;
                int cfd = accept(sfd, NULL, NULL);

                recv(cfd, &msg, sizeof(int), 0);

                if (msg == SERVER)
                {
                    recv(cfd, &remote_rooms_amount, sizeof(int), 0);
                    printf("remote_rooms_amount: %d\n", remote_rooms_amount);
                    recv(cfd, rooms, sizeof(*rooms) * remote_rooms_amount, 0);
                    printf("Received %d rooms.\n", remote_rooms_amount);
                    for (int k = 0; k < remote_rooms_amount; ++k)
                        printf("rooms[%d] = {fd_X: %d, fd_O: %d, move: %d}\n", k, rooms[k].fd_X, rooms[k].fd_O, rooms[k].move);
                    for (int k = 0; k < remote_rooms_amount; ++k)
                    {
                        int received = recv(cfd, game_grid[k][0], 3 * 3 * sizeof(int*) + sizeof(int), 0);
                        printf("Received grids (%d bytes).\n", received);
                    }
                    perror("recv");
                    print_grid(game_grid[0]);
                    close(cfd);
                    continue;
                }

                send(cfd, &servers_amount, sizeof(int), 0);
                send(cfd, servers, sizeof(*servers) * servers_amount, 0);
                send(cfd, &cfd, sizeof(int), 0);

                if (rooms_amount >= rooms_max && msg != RECONN)
                {
                    msg = OVERLOAD;
                    send(cfd, &msg, sizeof(int), 0);
                    close(cfd);
                    continue;
                }

                printf("cfd: %d\n", cfd);
                // fcntl(cfd, F_SETFL, O_NONBLOCK);
                ev.events = EPOLLIN | EPOLLET;
                ev.data.fd = cfd;
                epoll_ctl(epollfd, EPOLL_CTL_ADD, cfd, &ev);

                if (msg != RECONN)
                    for (int k = 0; k < rooms_max; ++k)
                    {
                        if (rooms[k].fd_X == -1)
                        {
                            rooms[k].fd_X = cfd;
                            msg = WAIT;
                            send(rooms[k].fd_X, &msg, sizeof(int), 0);
                            break;
                        }
                        else if (rooms[k].fd_O == -1)
                        {
                            rooms[k].fd_O = cfd;
                            ++rooms_amount;
                            msg = READY;
                            send(rooms[k].fd_X, &msg, sizeof(int), 0);
                            send(rooms[k].fd_O, &msg, sizeof(int), 0);

                            msg = YOU_X;
                            send(rooms[k].fd_X, &msg, sizeof(int), 0);
                            msg = YOU_O;
                            send(rooms[k].fd_O, &msg, sizeof(int), 0);

                            msg = ROOM;
                            send(rooms[k].fd_X, &msg, sizeof(int), 0);
                            send(rooms[k].fd_O, &msg, sizeof(int), 0);

                            send(rooms[k].fd_X, &k, sizeof(int), 0);
                            send(rooms[k].fd_O, &k, sizeof(int), 0);

                            rooms[k].move = rooms[k].fd_X;
                            sync_send();
                            break;
                        }
                    }
                else
                {
                    rooms_amount = remote_rooms_amount;

                    send(cfd, &cfd, sizeof(int), 0);
                    recv(cfd, &msg, sizeof(int), 0);
                    printf("=== Current rooms state ===\n");
                    for (int k = 0; k < rooms_amount; ++k)
                        printf("rooms[%d] = {fd_X: %d, fd_O: %d, move: %d}\n", k, rooms[k].fd_X, rooms[k].fd_O, rooms[k].move);
                    if (msg == RECONN_FAIL)
                    {
                        int old_cfd, room, player;
                        recv(cfd, &old_cfd, sizeof(int), 0);
                        recv(cfd, &room, sizeof(int), 0);
                        recv(cfd, &player, sizeof(int), 0);
                        printf("cfd: %d, old_cfd: %d, room: %d, player: %d\n", cfd, old_cfd, room, player);
                        for (int k = 0; k < rooms_amount; ++k)
                        {
                            if (rooms[k].fd_X == old_cfd && player == YOU_X && k == room)
                            {
                                rooms[k].fd_X = cfd;
                                if (rooms[k].move == old_cfd)
                                    rooms[k].move = cfd;
                                break;
                            }
                            else if (rooms[k].fd_O == old_cfd && player == YOU_O && k == room)
                            {
                                rooms[k].fd_O = cfd;
                                if (rooms[k].move == old_cfd)
                                    rooms[k].move = cfd;
                                break;
                            }
                        }
                        printf("=== Rooms state after adjusting ===\n");
                        for (int k = 0; k < rooms_amount; ++k)
                            printf("rooms[%d] = {fd_X: %d, fd_O: %d, move: %d}\n", k, rooms[k].fd_X, rooms[k].fd_O, rooms[k].move);
                    }
                }

                continue;
            }
            else
            {
                sync_send();
                memset(&str, 0, 500);

                recv(events[i].data.fd, str, 500, 0);
                printf("%d: %s", events[i].data.fd, str);

                if (strncmp(str, "/set", 4) == 0)
                {
                    int x, y, val;
                    sscanf(str, "%*s %d %d\n", &x, &y);
                    x -= 1; y -= 1;

                    for (int k = 0; k < rooms_amount; ++k)
                    {
                        int msg = 0;
                        printf("k: %d\n", k);
                        if (rooms[k].fd_X == events[i].data.fd)
                        {
                            if (rooms[k].fd_X == rooms[k].move)
                                val = X;
                            else
                            {
                                msg = SET_WAIT;
                                send(rooms[k].fd_X, &msg, sizeof(int), 0);
                                break;
                            }

                            if (set_grid_val(game_grid[k], x, y, val) == -1)
                            {
                                printf("ERROR: cell is not empty\n");
                                msg = CELL;
                                send(rooms[k].fd_X, &msg, sizeof(int), 0);
                            }
                            else
                            {
                                msg = SET_DONE;
                                send(rooms[k].fd_X, &msg, sizeof(int), 0);
                                msg = SET_MADE;
                                send(rooms[k].fd_O, &msg, sizeof(int), 0);
                                msg = GRID;
                                send(rooms[k].fd_X, &msg, sizeof(int), 0);
                                send(rooms[k].fd_X, game_grid[k][0], 3 * 3 * sizeof(int*) + sizeof(int), 0);

                                send(rooms[k].fd_O, &msg, sizeof(int), 0);
                                send(rooms[k].fd_O, game_grid[k][0], 3 * 3 * sizeof(int*) + sizeof(int), 0);

                                rooms[k].move = rooms[k].fd_O;
                            }
                        }
                        else if (rooms[k].fd_O == events[i].data.fd)
                        {
                            if (rooms[k].fd_O == rooms[k].move)
                                val = O;
                            else
                            {
                                msg = SET_WAIT;
                                send(rooms[k].fd_O, &msg, sizeof(int), 0);
                                break;
                            }

                            if (set_grid_val(game_grid[k], x, y, val) == -1)
                            {
                                printf("ERROR: cell is not empty\n");
                                msg = CELL;
                                send(rooms[k].fd_O, &msg, sizeof(int), 0);
                            }
                            else
                            {
                                msg = SET_DONE;
                                send(rooms[k].fd_O, &msg, sizeof(int), 0);
                                msg = SET_MADE;
                                send(rooms[k].fd_X, &msg, sizeof(int), 0);
                                msg = GRID;
                                send(rooms[k].fd_X, &msg, sizeof(int), 0);
                                send(rooms[k].fd_X, game_grid[k][0], 3 * 3 * sizeof(int*) + sizeof(int), 0);

                                send(rooms[k].fd_O, &msg, sizeof(int), 0);
                                send(rooms[k].fd_O, game_grid[k][0], 3 * 3 * sizeof(int*) + sizeof(int), 0);

                                rooms[k].move = rooms[k].fd_X;
                            }
                        }


                        int win_cond = check_grid_condition(game_grid[k]);
                        printf("k: %d, win_cond: %d\n", k, win_cond);
                        if (win_cond == X)
                        {
                            msg = GRID;
                            send(rooms[k].fd_X, &msg, sizeof(int), 0);
                            send(rooms[k].fd_X, game_grid[k][0], 3 * 3 * sizeof(int*) + sizeof(int), 0);

                            send(rooms[k].fd_O, &msg, sizeof(int), 0);
                            send(rooms[k].fd_O, game_grid[k][0], 3 * 3 * sizeof(int*) + sizeof(int), 0);

                            msg = WIN_X;
                            send(rooms[k].fd_X, &msg, sizeof(int), 0);
                            send(rooms[k].fd_O, &msg, sizeof(int), 0);

                            shutdown(rooms[k].fd_X, SHUT_RDWR);
                            shutdown(rooms[k].fd_O, SHUT_RDWR);
                            close(rooms[k].fd_X);
                            close(rooms[k].fd_O);
                            rooms[k].fd_X = -1;
                            rooms[k].fd_O = -1;
                            --rooms_amount;
                            dealloc_grid(game_grid[k]);
                            game_grid[k] = alloc_grid();
                        }
                        else if (win_cond == O)
                        {
                            msg = GRID;
                            send(rooms[k].fd_X, &msg, sizeof(int), 0);
                            send(rooms[k].fd_X, game_grid[k][0], 3 * 3 * sizeof(int*) + sizeof(int), 0);

                            send(rooms[k].fd_O, &msg, sizeof(int), 0);
                            send(rooms[k].fd_O, game_grid[k][0], 3 * 3 * sizeof(int*) + sizeof(int), 0);

                            msg = WIN_O;
                            send(rooms[k].fd_X, &msg, sizeof(int), 0);
                            send(rooms[k].fd_O, &msg, sizeof(int), 0);

                            shutdown(rooms[k].fd_X, SHUT_RDWR);
                            shutdown(rooms[k].fd_O, SHUT_RDWR);
                            close(rooms[k].fd_X);
                            close(rooms[k].fd_O);
                            rooms[k].fd_X = -1;
                            rooms[k].fd_O = -1;
                            --rooms_amount;
                            dealloc_grid(game_grid[k]);
                            game_grid[k] = alloc_grid();
                        }
                    }
                    sync_send();
                    continue;
                }
                else if (strcmp(str, "/grid\n") == 0)
                {
                    int msg = GRID;
                    for (int k = 0; k < rooms_amount; ++k)
                    {
                        printf("k: %d\n", k);
                        if (rooms[k].fd_X == events[i].data.fd || rooms[k].fd_O == events[i].data.fd)
                        {
                            print_grid(game_grid[k]);
                            send(events[i].data.fd, &msg, sizeof(int), 0);
                            send(events[i].data.fd, game_grid[k][0], 3 * 3 * sizeof(int*) + sizeof(int), 0);
                            break;
                        }
                    }
                    continue;
                }
                else if (strcmp(str, "/exit\n") == 0)
                {
                    int msg = EXIT;
                    shutdown(events[i].data.fd, SHUT_RDWR);
                    close(events[i].data.fd);
                    for (int k = 0; k < rooms_amount; ++k)
                    {
                        if (rooms[k].fd_X == events[i].data.fd)
                        {
                            rooms[k].fd_X = -1;

                            send(rooms[k].fd_O, &msg, sizeof(int), 0);
                            shutdown(rooms[k].fd_O, SHUT_RDWR);
                            close(rooms[k].fd_O);
                            rooms[k].fd_O = -1;
                            --rooms_amount;
                            dealloc_grid(game_grid[k]);
                            game_grid[k] = alloc_grid();
                        }
                        else if (rooms[k].fd_O == events[i].data.fd)
                        {
                            rooms[k].fd_O = -1;

                            send(rooms[k].fd_X, &msg, sizeof(int), 0);
                            shutdown(rooms[k].fd_X, SHUT_RDWR);
                            close(rooms[k].fd_X);
                            rooms[k].fd_X = -1;
                            --rooms_amount;
                            dealloc_grid(game_grid[k]);
                            game_grid[k] = alloc_grid();
                        }
                    }
                    break;
                }
                else if (strcmp(str, "/stop\n") == 0)
                {
                    int msg = STOP;
                    send_msg_all(&msg);

                    shutdown(events[i].data.fd, SHUT_RDWR);
                    close(events[i].data.fd);
                    exit(0);
                }

                int msg = STRING;
                for (int k = 0; k < rooms_amount; ++k)
                {
                    if (rooms[k].fd_X == events[i].data.fd)
                    {
                        send(rooms[k].fd_O, &msg, sizeof(int), 0);
                        send(rooms[k].fd_O, str, 500, 0);
                    }
                    else if (rooms[k].fd_O == events[i].data.fd)
                    {
                        send(rooms[k].fd_X, &msg, sizeof(int), 0);
                        send(rooms[k].fd_X, str, 500, 0);
                    }
                }
            }
        }
    }
    close(sfd);

    return 0;
}
