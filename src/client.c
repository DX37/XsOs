#include "game.h"

int sfd, player = 0;
pthread_t read_tid;
int **game_grid;
int current_server = 0, servers_amount, client_cfd, client_room;
list *servers;

void sig_handle()
{
    int msg = COMMAND;
    send(sfd, &msg, sizeof(int), 0);
    send(sfd, "/exit\n", 500, 0);
    dealloc_grid(game_grid);
    exit(0);
}

int init_conn(char* ip, int port)
{
    struct sockaddr_in sa;
    sfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sfd == -1)
    {
        perror("socket");
        return -1;
    }

    memset(&sa, 0, sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);

    if (inet_pton(AF_INET, ip, &sa.sin_addr) != 1)
    {
        perror("inet_pton");
        close(sfd);
        return -1;
    }

    if (connect(sfd, (struct sockaddr *)&sa, sizeof(sa)) != 0)
    {
        printf("ERROR: server is not present at specified address\n");
        perror("connect");
        close(sfd);
        return -1;
    }

    return 0;
}

void read_message()
{
    int msg = 0;
    char str[500];

    while (1)
    {
        memset(&str, 0, 500);
        if (recv(sfd, &msg, sizeof(int), 0) == -1)
            perr("recv msg");

        if (msg == SET_DONE)
            printf("\nMove completed. Now wait for opponent.\n");
        else if (msg == SET_MADE)
            printf("\nOpponent made a move. Now your turn.\n");
        else if (msg == SET_WAIT)
            printf("\nIt's not your move now. Wait for opponent.\n");
        else if (msg == CELL)
            printf("\nCell is not empty. Cannot set.\n");
        else if (msg == YOU_X)
        {
            printf("\nYou are \E[1;34mX\E[0m. Make your move.\n");
            player = YOU_X;
            print_grid(game_grid);
        }
        else if (msg == YOU_O)
        {
            printf("\nYou are \E[1;31mO\E[0m. Wait for opponent move.\n");
            player = YOU_O;
            print_grid(game_grid);
        }
        else if (msg == ROOM)
        {
            recv(sfd, &client_room, sizeof(int), 0);
            printf("Received room: %d\n", client_room);
        }
        else if (msg == WIN_X)
        {
            printf("\n\E[1;34mX\E[0ms win!\n");
            shutdown(sfd, SHUT_RDWR);
            close(sfd);
            exit(0);
        }
        else if (msg == WIN_O)
        {
            printf("\n\E[1;31mO\E[0ms win!\n");
            shutdown(sfd, SHUT_RDWR);
            close(sfd);
            exit(0);
        }
        else if (msg == GRID)
        {
            if (recv(sfd, game_grid[0], 3 * 3 * sizeof(int*) + sizeof(int), 0) == -1)
                perr("recv grid");
            printf("\n");
            if (player == YOU_X)
                printf("\nYou are \E[1;34mX\E[0m.\n");
            else if (player == YOU_O)
                printf("\nYou are \E[1;31mO\E[0m.\n");
            else
                printf("\nYou are undefined?\n");
            print_grid(game_grid);
            // printf("\n> ");
        }
        else if (msg == STRING)
        {
            if (recv(sfd, str, 500, 0) == -1)
                perr("recv string");

            if (player == YOU_X)
                printf("\n\E[1;31mO\E[0m: %s", str);
            else if (player == YOU_O)
                printf("\n\E[1;34mX\E[0m: %s", str);
            else
                printf("\nundefined: %s", str);

            // printf("\n> ");
        }
        else if (msg == EXIT)
        {
            printf("\nOpponent exited the game.\n");
            shutdown(sfd, SHUT_RDWR);
            close(sfd);
            exit(0);
        }
        else if (msg == STOP)
        {
            do
            {
                printf("\nServer %d not responding. Switching to next server...\n", current_server);
                shutdown(sfd, SHUT_RDWR);
                close(sfd);

                ++current_server;
                if (current_server >= servers_amount)
                {
                    printf("Next server not found. Exiting.\n");
                    shutdown(sfd, SHUT_RDWR);
                    close(sfd);
                    exit(0);
                }
            }
            while (init_conn(servers[current_server].ip, servers[current_server].port) != 0);

            msg = RECONN;
            send(sfd, &msg, sizeof(int), 0);

            recv(sfd, &servers_amount, sizeof(int), 0);
            servers = calloc(servers_amount, sizeof(*servers));

            recv(sfd, servers, sizeof(*servers) * servers_amount, 0);
            for (int i = 0; i < servers_amount; ++i)
            {
                if (current_server == i)
                    printf("%d) %s:%d (type: %d) <<<\n", i + 1, servers[i].ip, servers[i].port, servers[i].type);
                else
                    printf("%d) %s:%d (type: %d)\n", i + 1, servers[i].ip, servers[i].port, servers[i].type);
            }
            int tmp_cfd;
            recv(sfd, &tmp_cfd, sizeof(int), 0);
            printf("Received cfd: %d\n", tmp_cfd);
            if (tmp_cfd != client_cfd)
            {
                printf("Received cfd isn't the same before reconnecting. Adjusting...\n");
                msg = RECONN_FAIL;
                send(sfd, &msg, sizeof(int), 0);
                send(sfd, &client_cfd, sizeof(int), 0);
                send(sfd, &client_room, sizeof(int), 0);
                send(sfd, &player, sizeof(int), 0);
            }
            else
            {
                printf("Received cfd is the same before reconnecting.\n");
                msg = RECONN_DONE;
                send(sfd, &msg, sizeof(int), 0);
            }
            printf("\n");
        }
    }
}

int main(int argc, char** argv)
{
    signal(SIGINT, sig_handle);
    signal(SIGTERM, sig_handle);
    signal(SIGHUP, sig_handle);

    int msg;
    char command[500];

    char *ip = (argc > 1) ? argv[1] : NULL;
    int port = (argc > 2) ? atoi(argv[2]) : 7653;

    if (ip == NULL)
    {
        ip = calloc(16, sizeof(char));
        strcpy(ip, "127.0.0.1");
    }

    if (init_conn(ip, port) != 0)
        perr("init_conn");

    msg = CLIENT;
    send(sfd, &msg, sizeof(int), 0);

    recv(sfd, &servers_amount, sizeof(int), 0);
    servers = calloc(servers_amount, sizeof(*servers));

    recv(sfd, servers, sizeof(*servers) * servers_amount, 0);
    for (int i = 0; i < servers_amount; ++i)
    {
        if (current_server == i)
            printf("%d) %s:%d (type: %d) <<<\n", i + 1, servers[i].ip, servers[i].port, servers[i].type);
        else
            printf("%d) %s:%d (type: %d)\n", i + 1, servers[i].ip, servers[i].port, servers[i].type);
    }
    recv(sfd, &client_cfd, sizeof(int), 0);
    printf("Received cfd: %d\n", client_cfd);
    printf("\n");

    while (1)
    {
        if (recv(sfd, &msg, sizeof(int), 0) == -1)
            perr("initial recv");

        if (msg == WAIT)
        {
            printf("Waiting for pair...\n");
            continue;
        }
        else if (msg == READY)
        {
            printf("Pair connected!\n");
            break;
        }
        else if (msg == OVERLOAD)
        {
            printf("Server overloaded.\n");
            shutdown(sfd, SHUT_RDWR);
            close(sfd);
            return 0;
        }
        else if (msg == STOP)
        {
            printf("Server terminated.\n");
            shutdown(sfd, SHUT_RDWR);
            close(sfd);
            return 0;
        }
    }

    if (pthread_create(&read_tid, NULL, (void *)read_message, NULL) != 0)
    {
        perror("pthread_create");
        return -1;
    }

    game_grid = alloc_grid();

    while (1)
    {
        memset(&command, 0, 500);
        // printf("\n> ");
        fgets(command, 500, stdin);

        if (strcmp(command, "/grid\n") == 0)
        {
            send(sfd, command, 500, 0);
            continue;
        }
        else if (strncmp(command, "/set", 4) == 0)
        {
            int x, y;
            sscanf(command, "%*s %d %d\n", &x, &y);
            x -= 1; y -= 1;

            if (check_set_coords(x, y) == -1)
            {
                printf("ERROR: coords is out of game grid\n");
                continue;
            }

            send(sfd, command, 500, 0);
            continue;
        }
        else if (strcmp(command, "/help\n") == 0)
        {
            printf("/grid - print grid\n");
            if (player == YOU_X)
                printf("/set x y - set your \E[1;34mX\E[0m at grid's x,y coordinates\n");
            else if (player == YOU_O)
                printf("/set x y - set your \E[1;31mO\E[0m at grid's x,y coordinates\n");
            printf("/stop - stop server and quit from game\n");
            printf("/exit - exit room and quit from game\n");
            continue;
        }
        else if (strcmp(command, "/stop\n") == 0 || strcmp(command, "/exit\n") == 0)
        {
            send(sfd, command, 500, 0);
            break;
        }
        else if (strncmp(command, "/", 1) == 0)
        {
            printf("ERROR: wrong command\n");
            continue;
        }
        else if (strcmp(command, "\n") == 0)
        {
            printf("ERROR: enter command or string to chat\n");
            continue;
        }

        send(sfd, command, 500, 0);
    }

    pthread_kill(read_tid, SIGINT);
    dealloc_grid(game_grid);
    shutdown(sfd, SHUT_RDWR);
    close(sfd);

    return 0;
}
