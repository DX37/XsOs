#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/epoll.h>
#include <fcntl.h>
#include <pthread.h>
#include <signal.h>

#ifndef GAME_H
#define GAME_H

typedef struct
{
    char ip[15];
    int port;
    int type;
} list;

typedef enum {EMPTY, X, O} player_t;
typedef enum {COMMAND = 3756, CLIENT, ROOM, RECONN, RECONN_DONE, RECONN_FAIL, SERVER, STRING, GRID, STOP, OVERLOAD, WAIT, READY, EXIT, SET_DONE, SET_MADE, SET_WAIT, YOU_X, YOU_O, WIN_X, WIN_O, CELL} msg_t;

void perr(char*);
void print_grid(int**);
int set_grid_val(int**, int, int, int);
int check_set_coords(int, int);
void dealloc_grid(int**);
int** alloc_grid();
int check_grid_condition(int**);

#endif
